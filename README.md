# To Install on a system, run as root:
```
git clone --recursive https://gitlab.com/tstone2077/tlsbash.git /etc/tlsbash
echo "source /etc/tlsbash/common.bashrc" >> /etc/skel/.bashrc
echo "source /etc/tlsbash/common.bashrc" >> ~/.bashrc
```

# To Install for a user
```
git clone --recursive https://gitlab.com/tstone2077/tlsbash.git ~/.tlsbash
echo "source ~/.tlsbash/common.bashrc" >> ~/.bashrc
```

# To Install your own personal theme, you can edit:
~/.config/my.theme
```
LP_COLOR_USER_ROOT="$RED"
LP_COLOR_HOST="$YELLOW"
LP_COLOR_SSH="$YELLOW"
LP_COLOR_IN_MULTIPLEXER="$YELLOW"
LP_MARK_PREFIX="\n"
```