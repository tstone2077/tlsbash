#!/bin/bash

# if tmux is installed, it will set the config file (if it's not already set
# up) and start any existing tmux session
TLSBASH_ROOT="$( cd "$( dirname "${BASH_SOURCE}" )/.." && pwd )"

TERM_PROGRAMS_NO_TMUX="vscode"

tmux --version > /dev/null 2>&1
if test $? -eq 1
then
    if ! test -f ${HOME}/.tmux.conf
    then
        ln -s ${TLSBASH_ROOT}/tmux/tmux.conf ${HOME}/.tmux.conf
    fi
    if test -f ${TLSBASH_ROOT}/tmux/startTmux && \
       test -z $TMUX_PANE && \
       ! echo $TERM_PROGRAMS_NO_TMUX | grep $TERM_PROGRAM > /dev/null 2>&1
    then
        . ${TLSBASH_ROOT}/tmux/startTmux
    fi
fi
alias startTmux='bash ${TLSBASH_ROOT}/tmux/startTmux'

function takeover_tmux {
    if test "$TMUX" != ""
    then
        echo "This is a nested TMUX session.  Used Prefix - d to detach and rerun takeover"
        return
    fi

    # create a temporary session that displays the "how to go back" message
    tmp='takeover temp session'
    if ! tmux has-session -t "$tmp"; then
        echo "Creating new session: $tmp"
        tmux new-session -d -s "$tmp"
        tmux set-option -t "$tmp" set-remain-on-exit on
        tmux new-window -kt "$tmp":0 \
            'echo "Use Prefix + L (i.e. ^B L) to return to session."'
    fi

    # switch any clients attached to the target session to the temp session
    session="$1"
    if test "$session" == ""
    then
        echo "using $HOSTNAME as session name"
        session=$HOSTNAME
    fi
    for client in $(tmux list-clients -t "$session" | cut -f 1 -d :); do
        tmux switch-client -c "$client" -t "$tmp"
    done

    echo "Terminating session: $tmp"
    tmux kill-session -t "$tmp"
    # attach to the target session
    tmux attach -t "$session"
}
