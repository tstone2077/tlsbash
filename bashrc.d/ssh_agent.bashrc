#!/bin/bash

# If an ssh agent is setup, it will use that one. If not, it will start one
# up and use it

AGENT_FILE=$HOME/.ssh/agent.env
. $AGENT_FILE > /dev/null 2>&1

ssh-add -l > /dev/null 2>&1
retcode=$?
if test $retcode -eq 2
then
    # The agent isn't running
    ssh-agent -s > $AGENT_FILE
    . $AGENT_FILE
    retcode=1
fi
if ! test -f $HOME/.ssh/sshadd-askpass
then

  echo "$HOME/.ssh/sshadd-askpass not found. It should contain something like 'pass ssh_key_2020'"
else
  # The agent is running, but has no keys loaded
  chmod 700 ${HOME}/.ssh/sshadd-askpass
fi

alias loadkey='cat ~/.ssh/id_rsa | DISPLAY=0 SSH_ASKPASS='$HOME'/.ssh/sshadd-askpass ssh-add -t 300 - > /dev/null 2>&1'
