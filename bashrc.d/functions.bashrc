#!/bin/bash

# A Group of useful functions that can be used in bash
#
# install_vimrc : This will install Vundle.vim, install the default vimrc file
#                 and run the plugin installs
# get_keys_from : This will copy the id_rsa keys from a server to the local
#                 ${HOME}/.ssh directory
TLSBASH_ROOT="$( cd "$( dirname "${BASH_SOURCE}" )/.." && pwd )"

function install_vimrc {
        if [ ! -f ${HOME}/.vimrc ]; then
		git clone --recursive https://github.com/gmarik/Vundle.vim.git ${HOME}/.vim/bundle/Vundle.vim
		vim +PluginInstall +qall
        cp -R ${TLSBASH_ROOT}/vimrc ${HOME}/.vimrc
		vim +PluginInstall +qall
        else
                echo ".vimrc already exists"
        fi
}

function edit_lp_theme {
    if [ ! -f ${TLSBASH_ROOT}/my.theme ]; then
        cp ${TLSBASH_ROOT}/default.theme ${TLSBASH_ROOT}/my.theme
    fi
    $EDITOR ${TLSBASH_ROOT}/my.theme
}

# function to get ssh ids
function get_keys_from {
    # The first parameter should be the server we are copying from:
    # get_keys_from tstone@myserver
	if [ -z ${1+x} ]; then
		#no parameter passed is unset
		echo "You must pass in the host string from which to copy the keys"
		echo "For Example: get_keys_from tstone@myserver"
	else
		scp $1:~/.ssh/id_rsa* ${HOME}/.ssh/
		chmod 600 ${HOME}/.ssh/id_rsa*
	fi
}

function get_authorized_keys_from {
    # The first parameter should be the server we are copying from:
    # get_keys_from tstone@myserver
	if [ -z ${1+x} ]; then
		#no parameter passed is unset
		echo "You must pass in the host string from which to copy the keys"
		echo "For Example: get_keys_from tstone@myserver"
	else
		scp $1:~/.ssh/authorized_keys ${HOME}/.ssh/
	fi
}
