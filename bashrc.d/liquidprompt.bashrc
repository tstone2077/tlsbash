#!/bin/bash

# Setup liquidprompt by:
# 1. copying liquidpromptrc to the .config directory if it is not already there
# 2. Add lines to the liquidpromptrc to include themes if they exist, including:
#    A. ~/.config/liquidprompt/my.theme
#    B. ${TLSBASH_ROOT}/machine.theme or default.theme
# 3. source the liquidprompt script to start it
TLSBASH_ROOT="$( cd "$( dirname "${BASH_SOURCE}" )/.." && pwd )"

if [ ! -d ${HOME}/.config/liquidprompt ]; then
    mkdir ${HOME}/.config/liquidprompt
fi

if [ ! -f ${HOME}/.config/liquidpromptrc ]; then
    cp ${TLSBASH_ROOT}/liquidprompt/liquidpromptrc-dist ${HOME}/.config/liquidpromptrc
	cat >> ${HOME}/.config/liquidpromptrc <<EOF
if [ -f ${TLSBASH_ROOT}/machine.theme ]; then
	source ${TLSBASH_ROOT}/machine.theme
else
	source ${TLSBASH_ROOT}/default.theme
fi
if [ -f ~/.config/my.theme ]; then
	source ~/.config/my.theme
fi
EOF
fi
source ${TLSBASH_ROOT}/liquidprompt/liquidprompt
