if has("win32")
    source $VIMRUNTIME/mswin.vim
endif

hi comment ctermfg=blue
" define BadWhitespace before using in a match
highlight BadWhitespace ctermbg=red guibg=darkred

set autoindent
set fileformat=unix
set nocompatible              " required
set number
set tabstop=4
set softtabstop=4
set shiftwidth=4
set expandtab
filetype off                  " required

" set the runtime path to include Vundle and initialize
" set rtp+=~/.vim/bundle/Vundle.vim
" call vundle#begin()

" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
" Plugin 'gmarik/Vundle.vim'

" Add all your plugins here (note older versions of Vundle used Bundle instead of Plugin)
" Plugin 'tmhedberg/SimpylFold'
" Plugin 'vim-scripts/indentpython.vim'
" Plugin 'scrooloose/syntastic'
" Plugin 'nvie/vim-flake8'
" Plugin 'jnurmine/Zenburn'
" Plugin 'altercation/vim-colors-solarized'
" Plugin 'scrooloose/nerdtree'
" Plugin 'jistr/vim-nerdtree-tabs'
" Plugin 'kien/ctrlp.vim'
" Plugin 'tpope/vim-fugitive'
" Plugin 'tpope/vim-surround'
" Plugin 'bling/vim-airline'

" All of your Plugins must be added before the following line
" call vundle#end()            " required

" let g:nerdtree_tabs_open_on_console_startup=1

" filetype plugin indent on    " required
set encoding=utf-8

let python_highlight_all=1
syntax on

" let NERDTreeIgnore=['\.pyc$', '\~$'] "ignore files in NERDTree

if has('gui_running')
  set background=dark
  colorscheme koehler
else
  colorscheme koehler
endif
" call togglebg#map("<F5>")

au BufRead,BufNewFile *.css,*.js,*.py,*.pyw,*.c,*.h match BadWhitespace /\s\+$/

"Enable folding
set foldmethod=indent
set foldlevel=99
"nnoremap <space> za

au BufNewFile,BufRead *.py, *.sh, *.bash
    \ set textwidth=79 |

au BufNewFile,BufRead *.js
    \ set tabstop=2 |
    \ set softtabstop=2 |
    \ set shiftwidth=2 |
