#!/bin/bash
TLSBASH_ROOT="$( cd "$( dirname "${BASH_SOURCE}" )" && pwd )"

#Setup the necessary directories if they don't already exist
if ! test -d ${HOME}/.ssh
then
        mkdir -p ${HOME}/.ssh
fi
if ! test -d ${HOME}/.config
then
        mkdir -p ${HOME}/.config
fi
if ! test -f ${HOME}/.vimrc
then
        cp ${TLSBASH_ROOT}/vimrc ${HOME}/.vimrc
fi

# source all the *.bashrc files in the bashrc.d directory
for f in $(ls ${TLSBASH_ROOT}/bashrc.d/*.bashrc)
do
    source $f
done
